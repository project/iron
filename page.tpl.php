<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body class="<?php if ($sidebar_left) { print "withleft"; }?>">

  <div id="header">

    <?php if ($logo): ?>
      <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
        <img src="<?php print $logo; ?>" alt="<?php print t(Home); ?>" class="logo" />
      </a>
    <?php endif; ?>  

    <div class="site-name">
    <?php if ($site_name): ?>
      <h1><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
        <?php print $site_name ?>
          </a>
      </h1>
    <?php endif; ?>  

    </div>  
    <div class="site-slogan">
    <?php if ($site_slogan): ?>
      <h2>
        <?php print $site_slogan ?>
      </h2>
    <?php endif; ?>  
    </div>

    <?php print $search_box ?>

    <div class="primary-links">
      <?php if (isset($primary_links)): ?>
        <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
      <?php endif; ?>
    </div><!-- /primary-links -->

    <div class="secondary-links">
      <?php if (isset($secondary_links)): ?>
        <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
      <?php endif; ?>
    </div><!-- /secondary-links -->

  </div><!-- /header -->

  <div id="container">
    <?php print $breadcrumb ?>
    <div id="header-region">
      <?php print $header ?>
    </div>

    <div id="content">

      <div id="main">
        <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
        <?php if ($title): print '<h1 class="title">'. $title .'</h1>'; endif; ?>

	<div id="squeeze">  
          <?php if ($tabs): print '<div class="tabs">'. $tabs .'</div>'; endif; ?>  
          <?php print $help ?>
          <?php print $messages ?>
          <?php if ($content_top): print '<div id="content-top">'. $content_top .'</div>'; endif; ?>  
          <?php print $content ?>
          <?php if ($content_bottom): print '<div id="content-bottom">'. $content_bottom .'</div>'; endif; ?>
	</div><!-- /squeeze -->  

      </div><!-- /main -->

      <?php if ($sidebar_left): ?>
        <div id="sidebar">
          <?php print $sidebar_left ?>
        </div>
      <?php endif; ?>

    </div><!-- /content -->

  </div><!-- /container -->

  <div id="footer">
    <?php print $footer_message ?>
    <?php print $feed_icons ?>
  </div><!-- /footer -->

  <div id="copy">
    <p>Powered by <a href="http://www.drupal.org/">Drupal</a>&#160;&#8212; <!-- Don't remove, please, this comman line -->Design by <a href="http://drupal.mensh.ru/">drupal.mensh.ru</a>.</p>
  </div><!-- /copy -->

  <?php print $closure ?>  
  
</body>

</html>
